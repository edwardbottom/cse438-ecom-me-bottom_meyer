In the onCreate function, toast is a toast notification that pops up when the user hovers over it.

If the searchText toString is blank, then the function returns true and the toast notification pops up and tells the user
that they cannot submit blank text, However, if the function returns false, then a search is made.

Match parents matches the fragment's dimensions to the its parent activity and wrap_content wraps the content of the fragment to the activity
so that it can be properly displayed.

Various types of layouts include guideline horizontal, guideline vertical, linear layout horizontal, linear layout verticle,
frame layout verticle, frame layout horizontal, tablelayout, tablerow, and space.

Inflating a fragment attaches the fragment to the view that it is being presented on.

R is a static class used to list all of the available resources in an Android project.