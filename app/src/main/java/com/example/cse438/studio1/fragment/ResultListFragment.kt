package com.example.cse438.studio1.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_home.view.*
import android.widget.TextView
import com.example.cse438.studio1.R
import kotlinx.android.synthetic.main.fragment_result_list.*


@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String): Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // TODO: implement this function to inflate the fragment_result_list.xml file for the container;
        return inflater.inflate(R.layout.fragment_result_list, container, false)


        Log.d("Android:", "Incomplete")
//        return null
    }

    var queryString = query
    override fun onStart() {
        super.onStart()
        whats_new_text_list.setText("Search for: " + queryString)
    }
}